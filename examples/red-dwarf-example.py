#!/usr/bin/env python3

from scoly_api.client import Scoly
from datetime import datetime, timezone

client: Scoly = Scoly(
    scoly_url="http://127.0.0.1:8000",
    location="Red Dwarf",
    location_api_access_code="033589a1-c17d-45e0-bbaa-a5122300c9f3",
)
timestamp: str = datetime.now(tz=timezone.utc).replace(microsecond=0).isoformat() + "Z"
client.add_sensor_data(
    sensor="Rimmer",
    data={
        "timestamp": timestamp,
        "battery": 67,
        "presence": False,
        "celsius": 23.4,
        "lux": 45.6,
    },
)
client.flush()
