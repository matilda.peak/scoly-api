from datetime import datetime, timezone, timedelta
from scoly_api.client import Scoly


def test_default_constructor():
    client = Scoly(
        scoly_url="http://example.com:8000",
        location="Home",
        location_api_access_code="0000",
    )
    assert client._scoly_url == "http://example.com:8000"
    assert client._sensor_event_cache_size == 50
    assert client._sensor_event_cache_period == timedelta(seconds=360)
    assert client._last_transmit_success is None


def test_detailed_constructor():
    client = Scoly(
        scoly_url="http://example.com:8000",
        location="Home",
        location_api_access_code="0000",
        sensor_event_cache_size=10,
        sensor_event_cache_period_s=60,
    )
    assert client._scoly_url == "http://example.com:8000"
    assert client._sensor_event_cache_size == 10
    assert client._sensor_event_cache_period == timedelta(seconds=60)
    assert client._last_transmit_success is None


def test_default_sensor_event_cache():
    client = Scoly(
        scoly_url="http://example.com:8000",
        location="Home",
        location_api_access_code="0000",
    )
    assert client._sensor_cache_event_count == 0
    timestamp: str = (
        datetime.now(timezone.utc).replace(microsecond=0).isoformat() + "Z",
    )
    client.add_sensor_data(
        sensor="sensor1",
        data={"timestamp": timestamp, "temperature": 20},
    )
    assert client._sensor_cache_event_count == 1
    assert client._last_transmit_success is None


def test_sensor_event_without_cache():
    client = Scoly(
        scoly_url="http://example.com:8000",
        location="Home",
        location_api_access_code="0000",
        sensor_event_cache_size=0,
    )
    assert client._sensor_cache_event_count == 0
    timestamp: str = (
        datetime.now(timezone.utc).replace(microsecond=0).isoformat() + "Z",
    )
    client.add_sensor_data(
        sensor="sensor1",
        data={"timestamp": timestamp, "temperature": 20},
    )
    assert client._sensor_cache_event_count == 0
    assert client._last_transmit_success == False


def test_default_sensor_event_cache_flush():
    client = Scoly(
        scoly_url="http://example.com:8000",
        location="Home",
        location_api_access_code="0000",
    )
    timestamp: str = (
        datetime.now(timezone.utc).replace(microsecond=0).isoformat() + "Z",
    )
    client.add_sensor_data(
        sensor="sensor1",
        data={"timestamp": timestamp, "temperature": 20},
    )
    assert client._sensor_cache_event_count == 1
    client.flush()
    assert client._sensor_cache_event_count == 0
    assert client._last_transmit_success == False
